import { Injectable } from '@angular/core';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor() { }

  public fileURL = "";

  async uploadFile(file,folder) {
    console.log(file,folder);
    const contentType = file.type;
    const bucket = new S3(
      {
        "accessKeyId" : "AKIAQKZYYE2FUOXQ7FVI",
        "secretAccessKey" : "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",
        "region" : "ap-south-1"
      }
    );
    const params = {
      Bucket: 'stylelook',
      Key: folder + file.name,
      Body: file,
      ContentType: contentType
    };

    this.fileURL = await new Promise(resolve => {
      bucket.upload(params, function (err, data) {
        if (err) {
          console.log('There was an error uploading your file:', err);
        }
        console.log('Successfully uploaded file.', data);
        this.fileURL = data.Location;
        resolve(this.fileURL);
      });
    });
    return this.fileURL;    
  }
}
