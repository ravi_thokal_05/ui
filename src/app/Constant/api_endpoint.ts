export const api_endpoint = {

    // User endpoints

    // Auth Endpoint
    LOGIN : 'app/login',
    GET_OTP : 'app/get-otp/',
    SIGN_UP : 'app/signup/',
    USER_REGISTRATION : 'app/user-registration/',
    GOOGLE_LOGIN : 'app/google-login/',
    UPDATE_PASSWORD : 'app/update-password/',

    GET_USER : 'app/get-user/',
    GET_ADDRESS : 'app/get-address/',
    ADD_ADDRESS : 'app/add-address/',
    UPDATE_ADDRESS : 'app/update-address/',
    DELETE_ADDRESS : 'app/delete-address/',
    GET_OFFER : 'app/get-offer/',
    GET_BANNER : 'app/get-banner/',

    // user-shop
    GET_SHOP_LIST : 'app/get-shop-list/',
    GET_BRAND_LIST : 'app/get-brand-list/',
    GET_PRODUCT_LIST : 'app/get-product-list/',

    // Admin
    GET_BANNER_FOR_ADMIN : 'app/get-banner-admin/',
    DELETE_ADMIN_BANNER : 'app/delete-banner/',
    RESTORE_ADMIN_BANNER : 'app/delete-banner/',
    ADD_ADMIN_BANNER : 'app/add-banner/',
    UPDATE_ADMIN_BANNER : 'app/update-banner/',
    GET_USER_FOR_ADMIN : 'app/get-user-admin/',
    REFUND : 'app/refund/',
    SEND_EMAIL_TO_USER : '/app/send-email-to-user/',
    GET_SETTING : '/app/get-setting/',
    UPDATE_SETTING : '/app/update-setting/',
    GET_SHOP_LIST_FOR_ADMIN : '/app/get-shop-list-admin/',
    DELETE_SHOP : '/app/delete-shop/',
    SHOP_SIGNUP : '/app/signup/',
    SHOP_REGISTRATION : '/app/shop-registration/',
    DELETE_SHOP_LOGIN : '/app/delete-shop-login/',     //PERMANET DELETE SHOP LOGIN DON'T USE THIS API, IT IS USER IN SINGUP PROCESS FOR SHOP IF ANY ERROR OCCUR

    // Shop
    GET_SHOP_BANNER : '/app/get-banner-shop/',
    ADD_SHOP_BANNER : 'app/add-banner/',
    UPDATE_SHOP_BANNER : 'app/update-banner/',
    DELETE_SHOP_BANNER : 'app/delete-banner/',

};