import { Component, OnInit, ViewChild } from '@angular/core';
import { StoreService } from '../store.service';
import { UploadService } from 'src/app/services/upload.service';
import { FormGroup, FormControl, Validators, FormBuilder, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

  public loaderVisibility = false;
  public notificationAlert = false;

  public isRecordFound = false;
  public msg = "";
  public result = [];
  
  
  public banner = {
    "bannerId": "",
    "bannerLogo": ""
  }

  public fileToUpload: FileList;
  public bannerLogo = "";

  @ViewChild('closebutton1') closebutton1;
  @ViewChild('closebutton2') closebutton2;
  @ViewChild('closebutton3') closebutton3;
  @ViewChild('closebutton4') closebutton4;

  constructor(
    private fb: FormBuilder,
    private storeService: StoreService,
    private uploadService: UploadService
  ) { }

  ngOnInit() {

    // this.loaderVisibility = true;
    // this.storeService.getBannerForShop().subscribe({
    //   next: data => {
    //     this.loaderVisibility = false;
    //     this.notificationAlert = true;
    //     this.msg = data.msg;
    //     if (data.status == 200 && data.result && data.result.length > 0){          
    //       this.isRecordFound = true;
    //       this.result = data.result;
    //     }
    //   },
    //   error: error => {
    //     this.loaderVisibility = false;
    //     this.notificationAlert = true
    //     this.msg = "There was an error!"
    //   }
    // });

    this.addBannerForm = this.fb.group({
      bannerLogo: ['', Validators.required]
    });

    this.updateBannerForm = this.fb.group({
      bannerLogo: ['', Validators.required]
    });

  }

  setFile(files: FileList){
    this.fileToUpload = files;
  }

  setBanner(banner){
    this.banner = banner;
  }

  upload(files: FileList){
    this.loaderVisibility = true;
    this.uploadService.uploadFile(files.item(0),"banner/").then(bannerLogo => {
      this.loaderVisibility = false;
      this.bannerLogo = bannerLogo;
    });    
  }

  hideNotification(){
    this.notificationAlert = false;
  }

  addBannerForm: FormGroup;
  addBannerFormSubmitted = false;

  get f1() { return this.addBannerForm.controls; }  

  onAddBannerFormSubmit() {
    this.addBannerFormSubmitted = true;

    // stop here if form is invalid
    if (this.addBannerForm.invalid) {
      return;
    }

    this.loaderVisibility = true;
    this.uploadService.uploadFile(this.fileToUpload.item(0),"banner/" + localStorage.getItem('shopId') + "/")
    .then(bannerLogo => {
      this.bannerLogo = bannerLogo;

      var requestData = {
        "bannerLogo" :this.bannerLogo
      }

      this.storeService.addShopBanner(requestData).subscribe({
        next: data => {
          this.closebutton1.nativeElement.click();
          this.loaderVisibility = false;
          this.notificationAlert = true;
          this.msg = data.msg;
          if (data.status == 200 && data.result && data.result.length > 0){          
            this.isRecordFound = true;
            this.result = data.result;
          }
        },
        error: error => {
          this.loaderVisibility = false;
          this.notificationAlert = true
          this.msg = "There was an error!"
        }
      });

    })
    .catch((err) => { 
      this.loaderVisibility = false;
      this.msg = "Failed to upload banner logo!";
      this.notificationAlert = true
    });

  }

  updateBannerForm: FormGroup;
  updateBannerFormSubmitted = false;

  get f2() { return this.updateBannerForm.controls; }  

  onUpdateBannerFormSubmit() {
    this.updateBannerFormSubmitted = true;

    // stop here if form is invalid
    if (this.updateBannerForm.invalid) {
      return;
    }

    this.loaderVisibility = true;
    this.uploadService.uploadFile(this.fileToUpload.item(0),"banner/" + localStorage.getItem('shopId') + "/")
    .then(bannerLogo => {
      this.bannerLogo = bannerLogo;

      var requestData = {
        "bannerId" : this.banner.bannerId,
        "bannerLogo" : this.bannerLogo
      }

      this.storeService.updateShopBanner(requestData).subscribe({
        next: data => {
          this.closebutton2.nativeElement.click();
          this.loaderVisibility = false;
          this.notificationAlert = true;
          this.msg = data.msg;
          if (data.status == 200 && data.result && data.result.length > 0){          
            this.isRecordFound = true;
            this.result = data.result;
          }
        },
        error: error => {
          this.loaderVisibility = false;
          this.notificationAlert = true;
          this.msg = "There was an error!"
        }
      });

    })
    .catch((err) => { 
      this.loaderVisibility = false;
      this.notificationAlert = true;
      this.msg = "Failed to upload banner logo!";
      
    });

  }

  resetForm(){
    this.addBannerFormSubmitted = false;
    this.updateBannerFormSubmitted = false;
    this.addBannerForm = this.fb.group({
      bannerLogo: ['', Validators.required]
    });
    this.updateBannerForm = this.fb.group({
      bannerLogo: ['', Validators.required]
    });
  }

  deleteBanner(){

    this.loaderVisibility = true;

    var requestData = {
      "bannerId" : this.banner.bannerId,
      "isDeleted" : true
    }

    this.storeService.deleteShopBanner(requestData).subscribe({
      next: data => {
        this.closebutton3.nativeElement.click();
        this.loaderVisibility = false;
        this.notificationAlert = true;
        this.msg = data.msg;
        if (data.status == 200 && data.result && data.result.length > 0){          
          this.isRecordFound = true;
          this.result = data.result;
        }
      },
      error: error => {
        this.loaderVisibility = false;
        this.notificationAlert = true;
        this.msg = "There was an error!"
      }
    });

  }

  restoreBanner(){

    this.loaderVisibility = true;

    var requestData = {
      "bannerId" : this.banner.bannerId,
      "isDeleted" : false
    }

    this.storeService.deleteShopBanner(requestData).subscribe({
      next: data => {
        this.closebutton4.nativeElement.click();
        this.loaderVisibility = false;
        this.notificationAlert = true;
        this.msg = data.msg;
        if (data.status == 200 && data.result && data.result.length > 0){          
          this.isRecordFound = true;
          this.result = data.result;
        }
      },
      error: error => {
        this.loaderVisibility = false;
        this.notificationAlert = true;
        this.msg = "There was an error!"
      }
    });

  }

}
