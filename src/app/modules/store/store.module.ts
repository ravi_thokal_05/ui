import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { httpInterceptorProviders } from '../../core/http_interceptor';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';

import { StoreRoutingModule } from './store-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BannerComponent } from './banner/banner.component';
import { CategoryComponent } from './category/category.component';
import { OfferComponent } from './offer/offer.component';
import { OrderComponent } from './order/order.component';
import { ProductComponent } from './product/product.component';
import { ProfileComponent } from './profile/profile.component';
import { BrandComponent } from './brand/brand.component';

import { StoreService } from './store.service'; 
import { UploadService } from 'src/app/services/upload.service';

@NgModule({
  declarations: [
    DashboardComponent,
    BannerComponent, 
    CategoryComponent, 
    OfferComponent, 
    OrderComponent, 
    ProductComponent, 
    ProfileComponent, 
    BrandComponent
  ],
  imports: [
    CommonModule,
    StoreRoutingModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ 
    httpInterceptorProviders,
    StoreService,
    UploadService
  ]
})
export class StoreModule { }
