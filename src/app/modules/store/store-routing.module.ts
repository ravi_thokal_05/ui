import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from '../store/dashboard/dashboard.component';
import { BannerComponent } from './banner/banner.component';
import { CategoryComponent } from './category/category.component';
import { OfferComponent } from './offer/offer.component';
import { OrderComponent } from './order/order.component';
import { ProductComponent } from './product/product.component';
import { ProfileComponent } from './profile/profile.component';
import { BrandComponent } from './brand/brand.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent},
  { path: 'banner', component: BannerComponent},
  { path: 'brand', component: BrandComponent},
  { path: 'category', component: CategoryComponent},
  { path: 'offer', component: OfferComponent},
  { path: 'order', component: OrderComponent},
  { path: 'product', component: ProductComponent},
  { path: 'profile', component: ProfileComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StoreRoutingModule { }
