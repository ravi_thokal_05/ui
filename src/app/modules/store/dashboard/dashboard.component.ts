import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private _router: Router
  ) { }

  ngOnInit() {
  }

  goToModule(moduleName: any) {
    switch (moduleName) {
      case "Banner": {
        this._router.navigateByUrl('store/banner');
        break;
      }
      case "Brand": {
        this._router.navigateByUrl('store/brand');
        break;
      }
      case "Category": {
        this._router.navigateByUrl('store/category');
        break;
      }
      case "Offer": {
        this._router.navigateByUrl('store/offer');
        break;
      }
      case "Order": {
        this._router.navigateByUrl('store/order');
        break;
      }
      case "Product": {
        this._router.navigateByUrl('store/product');
        break;
      }
      case "Profile": {
        this._router.navigateByUrl('store/profile');
        break;
      }
      default: {
        this._router.navigateByUrl('login');
        break;
      }
    }
  }

}
