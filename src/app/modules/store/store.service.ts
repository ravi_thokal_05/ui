import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { api_endpoint } from 'src/app/Constant/api_endpoint';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private baseUrl = environment.apiBaseUrl;

  constructor(
    private http: HttpClient,
  ) { }

  getBannerForShop(): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.GET_SHOP_BANNER,
      {},
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  addShopBanner(data): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.ADD_SHOP_BANNER,
      data,
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  updateShopBanner(data): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.UPDATE_SHOP_BANNER,
      data,
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  deleteShopBanner(data): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.DELETE_SHOP_BANNER,
      data,
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

}
