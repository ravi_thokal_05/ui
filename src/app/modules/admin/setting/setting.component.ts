import { Component, OnInit, ViewChild } from '@angular/core';
import { AdminService } from '../admin.service';
import { FormGroup, FormControl, Validators, FormBuilder, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {

  @ViewChild('closebutton') closebutton;

  public loaderVisibility = false;
  public notificationAlert = false;
  public isEditable = false;
  public msg = "";
  public requestData = {
    emailFrom: "",
    emailBCC: [],
    contactUsPhone: "",
    contactUsEmail: "",
    instaLink: "",
    facebookLink: "",
    aboutUs: ""
  };
  public emailBCCText = ""
  public result = {
    emailFrom: "",
    emailBCC: [],
    contactUsPhone: "",
    contactUsEmail: "",
    instaLink: "",
    facebookLink: "",
    aboutUs: ""
  };

  settingForm: FormGroup;
  settingFormSubmitted = false;

  constructor(
    private adminService: AdminService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.loaderVisibility = true;
    this.adminService.getSetting().subscribe({
      next: data => {
        this.loaderVisibility = false;
        this.msg = data.msg;
        if (data.status == 200) {
          if (data.result)
            this.result = data.result;

            if(this.result.emailBCC && this.result.emailBCC.length>0){
              var i:number;
              for(i=0; i<this.result.emailBCC.length; i++){
                if(this.result.emailBCC.length == 1)
                  this.emailBCCText = this.emailBCCText + this.result.emailBCC[i];
                else if(this.result.emailBCC.length > 1 && i < (this.result.emailBCC.length-1) )
                  this.emailBCCText = this.emailBCCText + this.result.emailBCC[i] + ", ";
                else if (i == (this.result.emailBCC.length-1))
                this.emailBCCText = this.emailBCCText + this.result.emailBCC[i];
              }
            }

          this.notificationAlert = true;
        }
        else {
          this.notificationAlert = true
        }
      },
      error: error => {
        this.loaderVisibility = false;
        this.notificationAlert = true
        this.msg = "There was an error!"
      }
    });

    this.settingForm = this.fb.group({      
      emailFrom: ['', Validators.required],
      emailBCC: ['', Validators.required],
      contactUsPhone: ['', Validators.required],
      contactUsEmail: ['', Validators.required],
      instaLink: ['', Validators.required],
      facebookLink: ['', Validators.required],
      aboutUs: ['', Validators.required]
    });

    this.disableForm();

  }

   disableForm(){
     if(this.isEditable){
      this.settingForm.controls['emailFrom'].enable();
      this.settingForm.controls['emailBCC'].enable();
      this.settingForm.controls['contactUsPhone'].enable();
      this.settingForm.controls['contactUsEmail'].enable();
      this.settingForm.controls['instaLink'].enable();
      this.settingForm.controls['facebookLink'].enable();
      this.settingForm.controls['aboutUs'].enable();
     }
     else{
      this.settingForm.controls['emailFrom'].disable();
      this.settingForm.controls['emailBCC'].disable();
      this.settingForm.controls['contactUsPhone'].disable();
      this.settingForm.controls['contactUsEmail'].disable();
      this.settingForm.controls['instaLink'].disable();
      this.settingForm.controls['facebookLink'].disable();
      this.settingForm.controls['aboutUs'].disable();
     }
   }

  get f() { return this.settingForm.controls; }
  
  onSettingFormSubmit() {

    this.settingFormSubmitted = true;

    // stop here if form is invalid
    if (this.settingForm.invalid) {
        return;
    }

    this.requestData = {
      emailFrom: this.settingForm.value.emailFrom.trim(),
      emailBCC: [],
      contactUsPhone: this.settingForm.value.contactUsPhone.trim(),
      contactUsEmail: this.settingForm.value.contactUsEmail.trim(),
      instaLink: this.settingForm.value.instaLink.trim(),
      facebookLink: this.settingForm.value.facebookLink.trim(),
      aboutUs: this.settingForm.value.aboutUs.trim()
    };

    this.requestData.emailBCC = this.settingForm.value.emailBCC.trim().replace(/\s/g,'').split(',');

    this.loaderVisibility = true;
    this.adminService.updateSetting(this.requestData).subscribe({
      next: data => {
        this.loaderVisibility = false;
        this.msg = data.msg;
        if (data.status == 200) {
          if (data.result)
            this.result = data.result;

          if(this.result.emailBCC && this.result.emailBCC.length>0){
            var i:number;
            this.emailBCCText = "";
            for(i=0; i<this.result.emailBCC.length; i++){
              if(this.result.emailBCC.length == 1)
                this.emailBCCText = this.emailBCCText + this.result.emailBCC[i];
              else if(this.result.emailBCC.length > 1 && i < (this.result.emailBCC.length-1) )
                this.emailBCCText = this.emailBCCText + this.result.emailBCC[i] + ", ";
              else if (i == (this.result.emailBCC.length-1))
                this.emailBCCText = this.emailBCCText + this.result.emailBCC[i];
            }
          }
          this.closebutton.nativeElement.click();
          this.notificationAlert = true;
        }
        else {
          this.notificationAlert = true
        }
      },
      error: error => {
        this.closebutton.nativeElement.click();
        this.loaderVisibility = false;
        this.notificationAlert = true
        this.msg = "There was an error!"
      }
    });

    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.settingForm.value))
  }
  
  onClickEdit(){
    this.isEditable = true;
    this.settingForm.controls['emailFrom'].enable();
    this.settingForm.controls['emailBCC'].enable();
    this.settingForm.controls['contactUsPhone'].enable();
    this.settingForm.controls['contactUsEmail'].enable();
    this.settingForm.controls['instaLink'].enable();
    this.settingForm.controls['facebookLink'].enable();
    this.settingForm.controls['aboutUs'].enable();

    this.settingForm.patchValue({
      emailFrom: this.result.emailFrom,
      emailBCC: this.emailBCCText,
      contactUsPhone: this.result.contactUsPhone,
      contactUsEmail: this.result.contactUsEmail,
      instaLink: this.result.instaLink,
      facebookLink: this.result.facebookLink,
      aboutUs: this.result.aboutUs
    });

  }

  onClickClose(){

    this.settingFormSubmitted = false;

    this.settingForm.controls['emailFrom'].disable();
    this.settingForm.controls['emailBCC'].disable();
    this.settingForm.controls['contactUsPhone'].disable();
    this.settingForm.controls['contactUsEmail'].disable();
    this.settingForm.controls['instaLink'].disable();
    this.settingForm.controls['facebookLink'].disable();
    this.settingForm.controls['aboutUs'].disable();

    this.settingForm.patchValue({
      emailFrom: this.result.emailFrom,
      emailBCC: this.emailBCCText,
      contactUsPhone: this.result.contactUsPhone,
      contactUsEmail: this.result.contactUsEmail,
      instaLink: this.result.instaLink,
      facebookLink: this.result.facebookLink,
      aboutUs: this.result.aboutUs
    });

    this.isEditable = false;
  }

  hideNotification(){
    this.notificationAlert = false;
  }

}
