import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private _router: Router
  ) { }

  ngOnInit() {

  }

  goToModule(moduleName: any) {
    switch (moduleName) {
      case "Banner": {
        this._router.navigateByUrl('admin/banner');
        break;
      }
      case "Email": {
        this._router.navigateByUrl('admin/email');
        break;
      }
      case "Order": {
        this._router.navigateByUrl('admin/order');
        break;
      }
      case "Refund": {
        this._router.navigateByUrl('admin/refund');
        break;
      }
      case "Setting": {
        this._router.navigateByUrl('admin/setting');
        break;
      }
      case "Shop": {
        this._router.navigateByUrl('admin/shop');
        break;
      }
      case "User": {
        this._router.navigateByUrl('admin/user');
        break;
      }
      default: {
        this._router.navigateByUrl('login');
        break;
      }
    }
  }

}
