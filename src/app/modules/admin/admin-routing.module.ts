import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../admin/dashboard/dashboard.component';
import { BannerComponent } from './banner/banner.component';
import { EmailandrefundComponent } from './emailandrefund/emailandrefund.component';
import { SettingComponent } from './setting/setting.component';
import { OrderComponent } from './order/order.component';
import { ShopComponent } from './shop/shop.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent},
  { path: 'banner', component: BannerComponent},
  { path: 'email', component: EmailandrefundComponent},
  { path: 'refund', component: EmailandrefundComponent},
  { path: 'setting', component: SettingComponent},
  { path: 'order', component: OrderComponent},
  { path: 'shop', component: ShopComponent},
  { path: 'user', component: EmailandrefundComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
