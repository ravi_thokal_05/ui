import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';
import { FormGroup, FormControl, Validators, FormBuilder, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-emailandrefund',
  templateUrl: './emailandrefund.component.html',
  styleUrls: ['./emailandrefund.component.scss']
})
export class EmailandrefundComponent implements OnInit {

  public loaderVisibility = false;
  public notificationAlert = false;
  public msg = "No record found.";
  public isRecordFound = false;
  public isEmailModule = false;
  public isUserModule = false;
  public isRefundModule = false;
  public documentURL = "";
  public email = "";
  public userId = "";
  public requestData = {};
  public result = [];
  public headerText = "";
  public userDetail = {
    "userId" : "",
    "email" : ""
  }

  @ViewChild('closebutton1') closebutton1;
  @ViewChild('closebutton2') closebutton2;

  refundForm: FormGroup;
  refundFormSubmitted = false;

  emailForm: FormGroup;
  emailFormSubmitted = false;

  constructor(
    private router: Router,
    private adminService: AdminService,
    private fb: FormBuilder
  ) {  }

  ngOnInit() {

    this.searchForm = this.fb.group({      
      search: ['', Validators.required]
    });
    
    this.refundForm = this.fb.group({      
      refundAmount: ['', Validators.required]
    });

    this.emailForm = this.fb.group({      
      subject: ['', Validators.required],
      message: ['', Validators.required]
    });

    if (this.router.url.includes("email")) {
      this.isEmailModule = true;
      this.headerText = "Email"
    }
    if (this.router.url.includes("refund")) {
      this.isRefundModule = true;
      this.headerText = "Refund"
    }
    if (this.router.url.includes("user")) {
      this.isUserModule = true;
      this.headerText = "User"
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.refundForm.controls; }

  onRefundFormSubmit() {
    this.refundFormSubmitted = true;

    // stop here if form is invalid
    if (this.refundForm.invalid) {
        return;
    }

    this.requestData = {
      "userId": this.userDetail.userId,
      "refundAmount": this.refundForm.value.refundAmount.trim()
    }

    this.loaderVisibility = true;
    this.adminService.refund(this.requestData).subscribe({
      next: data => {
        this.closebutton1.nativeElement.click();
        this.loaderVisibility = false;
        this.msg = data.msg;
        if (data.status == 200) {
          if (data.result && data.result.length > 0)
            this.isRecordFound = true;
          else
            this.isRecordFound = false;
          this.result = data.result;
          this.notificationAlert = true;
        }
        else {
          this.notificationAlert = true
        }
      },
      error: error => {
        this.closebutton1.nativeElement.click();
        this.loaderVisibility = false;
        this.notificationAlert = true
        this.msg = "There was an error!"
      }
    });

    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.refundForm.value))
  } 

  get f1() { return this.emailForm.controls; }

  onEmailFormSubmit() {
    this.emailFormSubmitted = true;

    // stop here if form is invalid
    if (this.emailForm.invalid) {
        return;
    }

    this.requestData = {
      "userId": this.userDetail.userId,
      "to": this.userDetail.email,
      "subject": this.emailForm.value.subject.trim(),
      "message": this.emailForm.value.message.trim()
    }

    this.loaderVisibility = true;
    this.adminService.sendEmail(this.requestData).subscribe({
      next: data => {
        this.closebutton2.nativeElement.click();
        this.loaderVisibility = false;
        this.msg = data.msg;
        if (data.status == 200) {
          if (data.result && data.result.length > 0)
            this.isRecordFound = true;
          else
            this.isRecordFound = false;
          this.result = data.result;
          this.notificationAlert = true;
        }
        else {
          this.notificationAlert = true
        }
      },
      error: error => {
        this.closebutton2.nativeElement.click();
        this.loaderVisibility = false;
        this.notificationAlert = true
        this.msg = "There was an error!"
      }
    });

    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.emailForm.value))
  } 

  searchForm: FormGroup;
  searchFormSubmitted = false;
  get f2() { return this.searchForm.controls; }  
  onSearchFormSubmit() {
    this.searchFormSubmitted = true;

    // stop here if form is invalid
    if (this.searchForm.invalid) {
      return;
    }
    
    if (this.searchForm.value.search.includes('@')) {
      this.userId = ""
      this.email = this.searchForm.value.search.trim();
    }
    else {
      this.email = ""
      this.userId =this.searchForm.value.search.trim();
    }
    this.requestData = {
      "userId": this.userId,
      "email": this.email
    }

    this.loaderVisibility = true;
      this.adminService.getUserForAdmin(this.requestData).subscribe({
        next: data => {
          this.loaderVisibility = false;
          this.msg = data.msg;
          if (data.status == 200) {
            if (data.result && data.result.length > 0)
              this.isRecordFound = true;
            else
              this.isRecordFound = false;
            this.result = data.result;
            this.notificationAlert = true;
          }
          else {
            this.notificationAlert = true
          }
        },
        error: error => {
          this.loaderVisibility = false;
          this.notificationAlert = true
          this.msg = "There was an error!"
        }
      });
  }

  resetValidation() {
    this.searchFormSubmitted = false;
  }

  setUser(userDetail) {

    this.refundFormSubmitted = false;
    this.refundForm = this.fb.group({      
      refundAmount: ['', Validators.required]
    });

    this.emailFormSubmitted = false;
    this.emailForm = this.fb.group({      
      subject: ['', Validators.required],
      message: ['', Validators.required]
    });

    this.userDetail = userDetail;
  }

  hideNotification(){
    this.notificationAlert = false;
  }

}
