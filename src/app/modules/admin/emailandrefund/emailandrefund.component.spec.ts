import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailandrefundComponent } from './emailandrefund.component';

describe('EmailandrefundComponent', () => {
  let component: EmailandrefundComponent;
  let fixture: ComponentFixture<EmailandrefundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailandrefundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailandrefundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
