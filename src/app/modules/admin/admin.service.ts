import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { api_endpoint } from 'src/app/Constant/api_endpoint';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private baseUrl = environment.apiBaseUrl;
  constructor(
    private http: HttpClient,
  ) { }

  getBannerForAdmin(): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.GET_BANNER_FOR_ADMIN,
      {},
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  restoreAdminBanner(bannerId): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.RESTORE_ADMIN_BANNER,
      {
        "bannerId" : bannerId,
        "isDeleted": false
      },
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  deleteAdminBanner(bannerId): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.DELETE_ADMIN_BANNER,
      {
        "bannerId" : bannerId,
        "isDeleted": true
      },
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  addAdminBanner(bannerLogo): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.ADD_ADMIN_BANNER,
      {
        "bannerLogo": bannerLogo
      },
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  updateAdminBanner(banner): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.UPDATE_ADMIN_BANNER,
      banner,
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  getUserForAdmin(data): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.GET_USER_FOR_ADMIN,
      data,
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  refund(data): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.REFUND,
      data,
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  sendEmail(data): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.SEND_EMAIL_TO_USER,
      data,
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  getSetting(): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.GET_SETTING,
      {},
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  updateSetting(data): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.UPDATE_SETTING,
      data,
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  getShopListForAdmin(data): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.GET_SHOP_LIST_FOR_ADMIN,
      data,
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  deleteShop(data): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.DELETE_SHOP,
      data,
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  createShopLogin(data): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.SHOP_SIGNUP,
      data,
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  shopRegistration(data): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.SHOP_REGISTRATION,
      data,
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

  deleteShopLogin(data): Observable<any> {
    return this.http.post(
      this.baseUrl + api_endpoint.DELETE_SHOP_LOGIN,
      data,
      {
        headers: {
          "Authorization": localStorage.getItem('access_token')
        }
      }
    );
  }

}
