import { Component, OnInit, ViewChild } from '@angular/core';
import { AdminService } from '../admin.service';
import { UploadService } from 'src/app/services/upload.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

  public result = [];
  public msg = "";
  public isRecordFound = false;
  public notificationAlert = false;
  public loaderVisibility = false;
  public banner = {
    "bannerId": "",
    "bannerLogo": ""
  }
  public fileToUpload: File | null = null;
  public isFileSelected = false;
  public isValid = true;
  public bannerLogo = "";

  @ViewChild('closebutton1') closebutton1;
  @ViewChild('closebutton2') closebutton2;
  @ViewChild('closebutton3') closebutton3;
  @ViewChild('closebutton4') closebutton4;
  @ViewChild('fileInputAdd') fileInputAdd;
  @ViewChild('fileInputUpdate') fileInputUpdate;

  constructor(
    private adminService: AdminService,
    private uploadService: UploadService
  ) { }

  ngOnInit() {
    this.loaderVisibility = true;
    this.adminService.getBannerForAdmin().subscribe({
      next: data => {
        this.loaderVisibility = false;
        this.msg = data.msg;
        if (data.status == 200){
          if( data.result &&  data.result.length > 0)
            this.isRecordFound = true;
          else
            this.isRecordFound = false;
          this.result = data.result;
          this.notificationAlert = true;
        }
        else {
          this.notificationAlert = true
        }
      },
      error: error => {
        this.loaderVisibility = false;
        this.notificationAlert = true
        this.msg = "There was an error!"
      }
    });
  }

  resetFileSelection(){
    this.isFileSelected = false;
    this.isValid = true;
    this.fileInputAdd.nativeElement.value = "";
    this.fileInputUpdate.nativeElement.value = "";
  }

  setBanner(data){
    this.banner = data;
  }

  performCRUD(moduleName){
    switch (moduleName) {
      case "Add": {
        console.log(this.bannerLogo);
        if(this.isFileSelected){          
          this.loaderVisibility = true;
          this.adminService.addAdminBanner(this.bannerLogo).subscribe({
            next: data => {
              this.closebutton3.nativeElement.click();
              this.loaderVisibility = false;
              this.msg = data.msg;
              if (data.status == 200){
                this.isRecordFound = true;
                this.result = data.result;
                this.notificationAlert = true;
              }
              else {
                this.notificationAlert = true;
              }
            },
            error: error => {
              this.closebutton3.nativeElement.click();
              this.loaderVisibility = false;
              this.notificationAlert = true;
              this.msg = "There was an error!"
            }
          });
        }
        else{
          this.isValid = false;
        }      
        break;
          
      }
      case "Update": {
        if(this.isFileSelected){
          this.loaderVisibility = true;
          this.banner.bannerLogo = this.bannerLogo;
          this.adminService.updateAdminBanner(this.banner).subscribe({
            next: data => {
              this.closebutton4.nativeElement.click();
              this.loaderVisibility = false;
              this.msg = data.msg;
              if (data.status == 200){
                this.isRecordFound = true;
                this.result = data.result;
                this.notificationAlert = true;
              }
              else {
                this.notificationAlert = true;
              }
            },
            error: error => {
              this.closebutton4.nativeElement.click();
              this.loaderVisibility = false;
              this.notificationAlert = true;
              this.msg = "There was an error!"
            }
          });
        }
        else{
          this.isValid = false;
        }
        break;
      }
      case "Delete": {
        this.loaderVisibility = true;
        this.adminService.deleteAdminBanner(this.banner.bannerId).subscribe({
          next: data => {
            this.closebutton1.nativeElement.click();
            this.loaderVisibility = false;
            this.msg = data.msg;
            if (data.status == 200){
              this.isRecordFound = true;
              this.result = data.result;
              this.notificationAlert = true;
            }
            else {
              this.notificationAlert = true;
            }
          },
          error: error => {
            this.closebutton1.nativeElement.click();
            this.loaderVisibility = false;
            this.notificationAlert = true;
            this.msg = "There was an error!"
          }
        });        
        break;
      }
      case "Restore": {
        this.loaderVisibility = true;
        this.adminService.restoreAdminBanner(this.banner.bannerId).subscribe({
          next: data => {
            this.closebutton2.nativeElement.click();
            this.loaderVisibility = false;
            this.msg = data.msg;
            if (data.status == 200){
              this.isRecordFound = true;
              this.result = data.result;
              this.notificationAlert = true;
            }
            else {
              this.notificationAlert = true;
            }
          },
          error: error => {
            this.closebutton2.nativeElement.click();
            this.loaderVisibility = false;
            this.notificationAlert = true;
            this.msg = "There was an error!"
          }
        });
        break;
      }
      default: {
        break;
      }
    }
  }

  upload(files: FileList){
    if (files && files.length>0){
      this.isValid = true;
      this.isFileSelected = true;
      this.fileToUpload = files.item(0);

      this.loaderVisibility = true;
      this.uploadService.uploadFile(this.fileToUpload,"stylelook-banner/").then(bannerLogo => {
        this.loaderVisibility = false;
        this.bannerLogo = bannerLogo;
      });
    }
    else{
      this.isValid = false;
      this.isFileSelected = false;
    }
  }

  hideNotification(){
    this.notificationAlert = false;
  }

}
