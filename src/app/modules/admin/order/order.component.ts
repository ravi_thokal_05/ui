import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  public loaderVisibility = false;
  public notificationAlert = false;
  public msg = "";

  constructor(
    private adminService: AdminService,
  ) { }

  ngOnInit() {
  }

  hideNotification(){
    this.notificationAlert = false;
  }

}
