import { Component, OnInit, ViewChild } from '@angular/core';
import { AdminService } from '../admin.service';
import { UploadService } from 'src/app/services/upload.service';
import { FormGroup, FormControl, Validators, FormBuilder, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  public isRecordFound = true;
  public loaderVisibility = false;
  public notificationAlert = false;
  public msg = "";

  public fileToUpload : FileList;

  public shopLogo = "";

  public shopDetail = {
    "shopId": "",
    "shopName": "",
    "shopLogo": "",
    "shopRating": "",
    "shopOwnerName": "",
    "shopOwnerEmail": "",
    "shopOwnerContactNumber": "",
    "shopAddress": "",
    "shopLandmark": "",
    "latitude": "",
    "longitude": "",
    "deliveryCharge": "",
    "accountNumber": "",
    "IFSC": "",
    "instaLink": "",
    "facebookLink": ""
  }

  public getShopListRequestData  = {
    "searchData" : ""
  };

  public deleteShopRequestData = {
    "shopId" : "",
    "isDeleted" : false
  }

  public signupRequestData = {
    "username" : "",
    "rolename" : "shop"
  }

  public shopRegistrationRequestData = {
    "shopId": "",
    "shopName": "",
    "shopLogo": "",
    "shopRating": "",
    "shopOwnerName": "",
    "username": "",
    "shopOwnerContactNumber": "",
    "shopAddress": "",
    "shopLandmark": "",
    "latitude": "",
    "longitude": "",
    "deliveryCharge": "",
    "accountNumber": "",
    "IFSC": "",
    "instaLink": "",
    "facebookLink": ""
}

  public result = [];


  @ViewChild('closebutton1') closebutton1;
  @ViewChild('closebutton2') closebutton2;
  @ViewChild('closebutton3') closebutton3;
  @ViewChild('closebutton4') closebutton4;
  


  // Form --------------------------------------------------------------------------------------------------------------
  searchForm: FormGroup;
  searchFormSubmitted = false;
  get f1() { return this.searchForm.controls; }  
  onSearchFormSubmit() {
    this.searchFormSubmitted = true;

    // stop here if form is invalid
    if (this.searchForm.invalid) {
      return;
    }

    this.getShopListRequestData.searchData = this.searchForm.value.search.trim();
    this.getShopList(this.getShopListRequestData);

  }

  addShopForm: FormGroup;
  addShopFormSubmitted = false;
  get f2() { return this.addShopForm.controls; }  
  onAddShopFormSubmit() {
    this.addShopFormSubmitted = true;

    // stop here if form is invalid
    if (this.addShopForm.invalid) {
      return;
    }

    this.loaderVisibility = true;

    this.signupRequestData.username = this.addShopForm.value.username.trim();    
    this.adminService.createShopLogin(this.signupRequestData).subscribe({
      next: data => {        
        if (data.status == 200){

          this.uploadService.uploadFile(this.fileToUpload.item(0),"shop/" + data.cId + "_")
          .then(shopLogo => {
            this.shopLogo = shopLogo;
            this.shopRegistrationRequestData.shopId = data.cId;
            this.shopRegistrationRequestData.shopName = this.addShopForm.value.shopName.trim();
            this.shopRegistrationRequestData.shopLogo = this.shopLogo;
            this.shopRegistrationRequestData.shopRating  = this.addShopForm.value.shopRating.trim();
            this.shopRegistrationRequestData.shopOwnerName  = this.addShopForm.value.shopOwnerName.trim();
            this.shopRegistrationRequestData.username = this.addShopForm.value.username.trim();
            this.shopRegistrationRequestData.shopOwnerContactNumber = this.addShopForm.value.shopOwnerContactNumber.trim();
            this.shopRegistrationRequestData.shopAddress = this.addShopForm.value.shopAddress.trim();
            this.shopRegistrationRequestData.shopLandmark = this.addShopForm.value.shopLandmark.trim();
            this.shopRegistrationRequestData.latitude = this.addShopForm.value.latitude.trim();
            this.shopRegistrationRequestData.longitude = this.addShopForm.value.longitude.trim();
            this.shopRegistrationRequestData.deliveryCharge = this.addShopForm.value.deliveryCharge.trim();
            this.shopRegistrationRequestData.accountNumber = this.addShopForm.value.accountNumber.trim();
            this.shopRegistrationRequestData.IFSC = this.addShopForm.value.IFSC.trim();
            this.shopRegistrationRequestData.instaLink = this.addShopForm.value.instaLink.trim();
            this.shopRegistrationRequestData.facebookLink = this.addShopForm.value.facebookLink.trim();
            this.adminService.shopRegistration(this.shopRegistrationRequestData).subscribe({
              next: data => {          
                if (data.status == 200){
                  this.closebutton3.nativeElement.click();
                  this.loaderVisibility = false;
                  this.msg = data.msg;
                  this.notificationAlert = true;
                  if( data.result &&  data.result.length > 0){
                    this.isRecordFound = true;
                    this.result = data.result;
                  }
                  else if(this.result && this.result.length>0){
                    this.isRecordFound = true;
                  }
                  else{
                    this.isRecordFound = false;
                  }
                }
                else {
                  this.deleteShopLogin(this.shopRegistrationRequestData.shopId);
                  this.loaderVisibility = false;
                  this.msg = data.msg;
                  this.notificationAlert = true
                }
              },
              error: error => {
                this.deleteShopLogin(this.shopRegistrationRequestData.shopId);
                this.loaderVisibility = false;
                this.msg = "There was an error!"
                this.notificationAlert = true;              
              }
            });

          })
          .catch((err) => { 
            this.deleteShopLogin(this.shopRegistrationRequestData.shopId);
            this.loaderVisibility = false;
            this.msg = "Failed to upload shop logo!";
            this.notificationAlert = true
          });

        }
        else {
          this.loaderVisibility = false;
          this.msg = data.msg;
          this.notificationAlert = true
        }
      },
      error: error => {
        this.loaderVisibility = false;
        this.notificationAlert = true;
        this.msg = "There was an error!"
      }
    });
  }

  
  updateShopForm: FormGroup;
  updateShopFormSubmitted = false;
  get f3() { return this.addShopForm.controls; }  
  onUpdateShopFormSubmit() {
    this.updateShopFormSubmitted = true;

    // stop here if form is invalid
    if (this.addShopForm.invalid) {
      return;
    }
  }
  // -------------------------------------------------------------------------------------------------------------------

  constructor(
    private adminService: AdminService,
    private fb: FormBuilder,
    private uploadService: UploadService
  ) { }

  ngOnInit() {

    // Form Validation---------------------------------------------------------------------------------------------------
    this.searchForm = this.fb.group({      
      search: ['', Validators.required]
    });

    this.addShopForm = this.fb.group({
      shopName: ['', Validators.required],
      shopOwnerName: ['', Validators.required],
      username: ['', Validators.required],
      shopOwnerContactNumber: ['', Validators.required],
      shopAddress: ['', Validators.required],
      shopLandmark: ['', Validators.required],
      latitude: ['', Validators.required],
      longitude: ['', Validators.required],
      deliveryCharge: ['', Validators.required],
      shopRating: ['', Validators.required],
      accountNumber: ['', Validators.required],
      IFSC: ['', Validators.required],
      instaLink: ['', Validators.required],
      facebookLink: ['', Validators.required],
      shopLogo: ['', Validators.required],
    });

    this.updateShopForm = this.fb.group({
      shopName: ['', Validators.required],
      shopOwnerName: ['', Validators.required],
      shopOwnerEmail: ['', Validators.required],
      shopOwnerContactNumber: ['', Validators.required],
      shopAddress: ['', Validators.required],
      shopLandmark: ['', Validators.required],
      latitude: ['', Validators.required],
      longitude: ['', Validators.required],
      deliveryCharge: ['', Validators.required],
      shopRating: ['', Validators.required],
      accountNumber: ['', Validators.required],
      IFSC: ['', Validators.required],
      instaLink: ['', Validators.required],
      facebookLink: ['', Validators.required],
    });
    // -----------------------------------------------------------------------------------------------------------------
    
    this.getShopList(this.getShopListRequestData)
  }

  getShopList(requestData){
    this.loaderVisibility = true;
    this.adminService.getShopListForAdmin(requestData).subscribe({
      next: data => {
        this.loaderVisibility = false;
        this.msg = data.msg;
        if (data.status == 200){
          if( data.result &&  data.result.length > 0){
            this.isRecordFound = true;
            this.result = data.result;
          }
          else if(this.result && this.result.length>0){
            this.isRecordFound = true;
          }
          else{
            this.isRecordFound = false;
          }
          this.notificationAlert = true;
        }
        else {
          this.notificationAlert = true
        }
      },
      error: error => {
        this.loaderVisibility = false;
        this.notificationAlert = true;
        this.msg = "There was an error!"
      }
    });
  }

  setShop(shopDetail){
    this.shopDetail = shopDetail;
  }

  resetValidation(){
    this.searchFormSubmitted = false;
  }

  onclickDelete(){
    this.deleteShopRequestData = {
      "shopId" : this.shopDetail.shopId,
      "isDeleted": true
    }
    this.deleteShop();
  }

  onClickRestore(){
    this.deleteShopRequestData = {
      "shopId" : this.shopDetail.shopId,
      "isDeleted": false
    }
    this.deleteShop();
  }

  deleteShop(){
    this.loaderVisibility = true;
    this.adminService.deleteShop(this.deleteShopRequestData).subscribe({
      next: data => {
        this.closebutton1.nativeElement.click();
        this.closebutton2.nativeElement.click();
        this.loaderVisibility = false;
        this.msg = data.msg;
        if (data.status == 200){
          if( data.result &&  data.result.length > 0){
            this.isRecordFound = true;
            this.result = data.result;
          }
          else if(this.result && this.result.length>0){
            this.isRecordFound = true;
          }
          else{
            this.isRecordFound = false;
          }
          this.notificationAlert = true;
        }
        else {
          this.notificationAlert = true
        }
      },
      error: error => {
        this.closebutton1.nativeElement.click();
        this.closebutton2.nativeElement.click();
        this.loaderVisibility = false;
        this.notificationAlert = true;
        this.msg = "There was an error!"
      }
    });
  }

  onClickAdd(){
    this.addShopFormSubmitted = false;
    this.addShopForm = this.fb.group({
      shopName: ['', Validators.required],
      shopOwnerName: ['', Validators.required],
      username: ['', Validators.required],
      shopOwnerContactNumber: ['', Validators.required],
      shopAddress: ['', Validators.required],
      shopLandmark: ['', Validators.required],
      latitude: ['', Validators.required],
      longitude: ['', Validators.required],
      deliveryCharge: ['', Validators.required],
      shopRating: ['', Validators.required],
      accountNumber: ['', Validators.required],
      IFSC: ['', Validators.required],
      instaLink: ['', Validators.required],
      facebookLink: ['', Validators.required],
      shopLogo: ['', Validators.required],
    });
  }

  setFile(files: FileList){
    this.fileToUpload = files;
  }

  deleteShopLogin(shopId){
    this.adminService.deleteShopLogin({"shopId":shopId}).subscribe({
      next: data => {},
      error: error => {}
    });
  }

  hideNotification(){
    this.notificationAlert = false;
  }

  setShopAndUpdateShopForm(shopDetail){
    this.shopDetail = shopDetail;

    this.updateShopForm.controls['shopName'].disable();
    this.updateShopForm.controls['shopOwnerName'].disable();
    this.updateShopForm.controls['shopOwnerEmail'].disable();
    this.updateShopForm.controls['shopOwnerContactNumber'].disable();
    this.updateShopForm.controls['shopAddress'].disable();
    this.updateShopForm.controls['shopLandmark'].disable();
    this.updateShopForm.controls['latitude'].disable();
    this.updateShopForm.controls['longitude'].disable();
    this.updateShopForm.controls['deliveryCharge'].disable();
    this.updateShopForm.controls['shopRating'].disable();
    this.updateShopForm.controls['accountNumber'].disable();
    this.updateShopForm.controls['IFSC'].disable();
    this.updateShopForm.controls['instaLink'].disable();
    this.updateShopForm.controls['facebookLink'].disable();


    this.updateShopForm.patchValue({
      shopName: this.shopDetail.shopName,
      shopOwnerName: this.shopDetail.shopOwnerName,
      shopOwnerEmail: this.shopDetail.shopOwnerEmail,
      shopOwnerContactNumber: this.shopDetail.shopOwnerContactNumber,
      shopAddress: this.shopDetail.shopAddress,
      shopLandmark: this.shopDetail.shopLandmark,
      latitude: this.shopDetail.latitude,
      longitude: this.shopDetail.longitude,
      deliveryCharge: this.shopDetail.deliveryCharge,
      shopRating: this.shopDetail.shopRating,
      accountNumber: this.shopDetail.accountNumber,
      IFSC: this.shopDetail.IFSC,
      instaLink: this.shopDetail.instaLink,
      facebookLink: this.shopDetail.facebookLink
    });
  }
}
