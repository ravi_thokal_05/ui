import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { httpInterceptorProviders } from '../../core/http_interceptor';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';


import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from '../admin/dashboard/dashboard.component';
import { BannerComponent } from './banner/banner.component';

import { AdminService } from './admin.service';
import { UploadService } from 'src/app/services/upload.service';
import { EmailandrefundComponent } from './emailandrefund/emailandrefund.component';
import { SettingComponent } from './setting/setting.component';
import { OrderComponent } from './order/order.component';
import { ShopComponent } from './shop/shop.component';

@NgModule({
  declarations: [
    DashboardComponent,
    BannerComponent,
    EmailandrefundComponent,
    SettingComponent,
    OrderComponent,
    ShopComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    AdminRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ 
    httpInterceptorProviders,
    AdminService,
    UploadService
  ]
})
export class AdminModule { }
