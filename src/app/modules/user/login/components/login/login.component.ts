import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import {UserService} from 'src/app/core/services/user.service';
import { LoginService } from '../../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

 loginForm: FormGroup;
 show = 'hide';
 hide = true;
  constructor(
    private fb: FormBuilder,
    private route: Router,
    private userService: UserService,
    private loginService: LoginService
  ) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      pass: [
          '',
          [
              Validators.required,
              Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)
          ]
      ]
  });
   }

  ngOnInit(): void {
  }
  login() {
    console.log('hi');
    const payload = {
      username: '4aksli@gmail.com',
      password: '123'
    };
    // this.userService.login(payload).subscribe(result=>{});
    // this.route.navigate(['/dashboard']);
    this.loginService.login(payload).subscribe(result => {
      console.log('result --->', result);
    });
  }
  social() {}
}
