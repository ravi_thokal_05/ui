import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private baseUrl = environment.apiBaseUrl;
  constructor(
    private http: HttpClient,
  ) {}

  login(payload: any): Observable<any> {
    console.log('baseUrl----', this.baseUrl + '/app/login');
    return this.http.post<any>(this.baseUrl + '/app/login/', payload);
  }

  // getOTP(payload: any): Observable<any> {
  //   console.log('baseUrl', this.baseUrl + '/app/login/');
  //   return this.http.get(this.baseUrl + '/app/login/', payload);
  // }
}
