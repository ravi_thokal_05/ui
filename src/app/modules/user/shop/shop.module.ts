import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopComponent } from './shop.component';
import { RouterModule } from '@angular/router';
import { ShopRoutingModule } from './shop-routing.module';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { SharedModule } from '../../shared/shared.module';
@NgModule({
  declarations: [
    ShopComponent,
    ProductDetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ShopRoutingModule,
    SharedModule
  ],
  // exports: [ShopComponent]
})
export class ShopModule { }
