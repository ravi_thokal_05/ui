import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './modules/user/login/login.module#LoginModule',
  },
  {
    path: 'home',
    loadChildren: './modules/user/dashboard/dashboard.module#DashboardModule',
  },
  {
    path: 'shop',
    loadChildren: './modules/user/shop/shop.module#ShopModule',
  },
  {
    path: 'admin',
    loadChildren: './modules/admin/admin.module#AdminModule',
  },
  {
    path: 'store',
    loadChildren: './modules/store/store.module#StoreModule',
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
